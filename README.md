## Interview assignment

Welcome to BuyMed coding challenge!

Firstly, We're excited that you're interested in joining the BuyMed. Below are the requirements and explanations for the
challenge.

----------
**Documents**

* UI design:            https://www.figma.com/file/ADr9J48sKJaGD57eO89Nf9/Interview-assignment
* API documentation:    https://documenter.getpostman.com/view/6404626/TzY1icUf
* The public link:      https://www.getpostman.com/collections/08c73d57518864b82064

----------
**Requirement**
* Username:
    * at least 5 characters
    * only alphabet letters, numbers and accents: _ . are accepted
    * lowercase
* Password:
    * at least 8 characters with 1 uppercase, 1 lowercase, 1 number
    * only alphabet letters, numbers and accents: ! @ # $ % _ are accepted
* Email:
    * valid email format
* Class:
    * INTERNSHIP
    * FRESHER
    * JUNIOR
* Sex:
    * MALE
    * FEMALE
    * OTHER

----------
**Your assignment**
* Fork this repository and show your development with a pull/merge request. **(This is required)**
* Initialize the project based on `create-react-app` or `create-next-app` (choose which you are better)
* Place a README  on how to run, what is missing, what else you want to improve but don't have enough time
* Try your best on completing the forms for the feature `Sign-up and Login`
* Fork the API collection for using

*Optional*
* Show your information in the main page when login successfully
* Complete the feature `Forgot and reset password`<br/>
  (since this is just a simple assignment, we will use the default OTP of 24680 instead of receiving an email)

----------
Don't worry if you can't complete the challenge in time. Just do your best mindfully.
If you can't fully complete the challenge, please note the completed features.
